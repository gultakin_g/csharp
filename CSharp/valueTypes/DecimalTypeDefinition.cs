﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.valueTypes
{
    public class DecimalTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            decimal number;

            //Definition - 2
            decimal value1, value2, value3;

            //Definition - 3
            decimal minValue = -7.9m, maxValue = 7.9m;
            Console.WriteLine("Min value decimal: " + minValue +
                "\nMax value decimal: " + maxValue);

            //Definition - 4
            decimal defaultValue = 0.0m;
            decimal decValue = 8844.844884m;
            Console.WriteLine("Default value decimal: {0} \nValue decimal: {1}", 
                defaultValue, decValue);
        }
    }
}
