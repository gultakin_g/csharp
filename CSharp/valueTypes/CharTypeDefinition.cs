﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.valueTypes
{
    public class CharTypeDefinition
    {
        public void Define()
        {
            // Definition - 1
            char apostrophe;

            //Definition - 2
            char apperLetter, lowerLetter, equalSymbol;

            //Definition - 3
            char arithmeticSymbol = '+', letterSymbol = 'A';
            Console.WriteLine("Arithmetic symbol: {0}  \nLetter symbol: {1}",
                               arithmeticSymbol, letterSymbol);


            //Definition - 4
            char dollarSymbol = '$';
            Console.WriteLine("Dollar symbol: {0}", dollarSymbol);
            char ampersand = '&';
            Console.WriteLine("Ampersand symbol: " + ampersand);
        }
    }
}
