﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.valueTypes
{
    public class IntTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            int number;

            //Definition - 2
            int value1, value2, value3, value4;

            //Definition - 3
            int minValue = -2147483648, maxValue = 2147483647;
            Console.WriteLine("Min value int: {0}  \nMax value int: {1}",
                minValue, maxValue);

            //Definition - 4
            int defaultValue = 0;
            int value = 38484;
            Console.WriteLine("Default value int: "+ defaultValue +
                "\nValue int: "+value);

        }
    }
}
