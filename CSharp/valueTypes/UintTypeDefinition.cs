﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.valueTypes
{
    public class UintTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            uint number;

            //Definition - 2
            uint value1, value2, value3;

            //Definition - 3
            uint minValue = 0, maxValue = 4294967295;
            Console.WriteLine("Min value uint: {0}  \nMax value uint: {1}",
                minValue, maxValue);

            //Definition - 4
            uint defaultValue = 0;
            uint value = 37477;
            Console.WriteLine("Default value uint: " + defaultValue +
                "\nValue uint: " + value);

        }
    }
}
