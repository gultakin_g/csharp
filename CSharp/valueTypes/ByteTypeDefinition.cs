﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.valueTypes
{
    public class ByteTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            byte number;

            //Definition - 2
            byte value1, value2;

            //Definition - 3
            byte minvalue = 0, maxValue = 255;
            Console.WriteLine("Min value byte: " + minvalue + "\nMax value byte: " + maxValue);

            //Definition - 4
            byte bytValue = 134;
            Console.WriteLine("Value byte: " + bytValue);
            byte defaultValue = 0;
            Console.WriteLine("Defalt value byte:" + defaultValue);
        }
    }
}
