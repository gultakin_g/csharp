﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.valueTypes
{
    public class ShortTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            short number;

            //Definition - 2
            short value1, value2, value3;

            //Definition - 3
            short minValue = -32768, maxValue = 32767;
            Console.WriteLine("Min value short: {0} \nMax value short: {1}",
                minValue, maxValue);

            //Definition - 4
            short defaultValue = 0;
            Console.WriteLine("Default value short: {0}", defaultValue);
            short shortValue = 569;
            Console.WriteLine("Value short: " + shortValue);
        }
    }
}
