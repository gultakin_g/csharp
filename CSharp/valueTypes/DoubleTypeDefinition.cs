﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.valueTypes
{
    public class DoubleTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            double number;

            //Definition - 2
            double value1, value2, value3;

            //Definition - 3
            double minValue = -5.2, maxValue = 1.7;
            Console.WriteLine("Min value double: {0}" +
                " \nMax value double: {1}",
                minValue, maxValue);

            //Definition - 4
            double defaultValue = 0;
            Console.WriteLine("Default value: " + defaultValue);
            double value = 838383.74;
            Console.WriteLine("Value sbyte: {0}", value);
        }
    }
}
