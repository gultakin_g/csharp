﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.valueTypes
{
    public enum WeekDays
    {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    }

    public class EnumTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            WeekDays weekDay;

            //Definition - 2
            WeekDays value1, value2, value3;

            //Definition - 3
            WeekDays minValue = WeekDays.Monday, maxValue = WeekDays.Sunday;
            Console.WriteLine("Min value enum: " + minValue +
                "\nMax value enum: " + maxValue);

            //Definition - 4
            WeekDays defaultValue = WeekDays.Monday;
            WeekDays lastValue = (WeekDays)6;
            Console.WriteLine("Default value enum: {0} \nLast value enum: {1}",
                defaultValue, lastValue);
        }
    }
   
}
