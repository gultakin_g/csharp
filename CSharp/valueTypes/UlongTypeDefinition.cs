﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.valueTypes
{
    public class UlongTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            ulong number;

            //Definition - 2
            ulong value1, value2, value3;

            //Definition - 3
            ulong minValue = 0, maxValue = 18446744073709551615;
            Console.WriteLine("Min value ulong: {0}  \nMax value ulong: {1}",
                minValue, maxValue);

            //Definition - 4
            ulong defaultValue = 0;
            ulong value = 94440;
            Console.WriteLine("Default value ulong: " + defaultValue +
                "\nValue ulong: " + value);

        }
    }
}
