﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.valueTypes
{
    public class UshortTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            ushort number;

            //Definition - 2
            ushort value1, value2, value3;

            //Definition - 3
            ushort minValue = 0, maxValue = 65535;
            Console.WriteLine("Min value ushort: {0} \nMax value ushort: {1}",
                minValue, maxValue);

            //Definition - 4
            ushort defaultValue = 0;
            ushort value = 2335;
            Console.WriteLine("Default value ushort: " + defaultValue +
                "\nValue ushort: " + value);
        }
    }
}
