﻿using System;

namespace CSharp.valueTypes
{
    public class BooleanTypeDefinition
    {
        public void Define()
        {
            // Defination 1
            bool isChecked;

            // Defination 2
            bool isDog, isCat, isBird;

            // Defination 3
            bool hasValue = true, areProgrammers = false;
            Console.WriteLine("Has value: " + hasValue +
                "\nAre Programmers: " + areProgrammers);

            // Defination 4 
            bool isMan = true;
            bool isWoman = false;
            Console.WriteLine("Is man: {0}  \nIs Women: {1}", isMan, isWoman);
        }
    }
}
