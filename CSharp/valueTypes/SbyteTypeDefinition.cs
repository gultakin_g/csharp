﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.valueTypes
{
    public class SbyteTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            sbyte number;

            //Definition - 2
            sbyte value1, value2;

            //Definition - 3
            sbyte minValue = -128, maxValue = 127;
            Console.WriteLine("Min value sbyte: {0}" +
                " \nMax value sbyte: {1}",
                minValue, maxValue);

            //Definition  -4
            sbyte defaultValue = 0;
            Console.WriteLine("Default Value sbyte: " + defaultValue);

            sbyte sbytValue = 45;
            Console.WriteLine("Value sbyte: {0}", sbytValue);
        }
    }
}
