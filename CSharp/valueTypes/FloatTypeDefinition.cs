﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.valueTypes
{
    public class FloatTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            float number;

            //Definition - 2
            float value1, value2, value3;

            //Definition - 3
            float minValue = -3.4f, maxValue = 3.4f;
            Console.WriteLine("Min value float: {0}  \nMax value float: {1}",
                minValue, maxValue);

            //Definition - 4
            float defaultValue = 0.0f;
            Console.WriteLine("Default value float: " + defaultValue);

            float value = -4484.5f;
            Console.WriteLine("Default value float: " + value);
        }
    }
}
