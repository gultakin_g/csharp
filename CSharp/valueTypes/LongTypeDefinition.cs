﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.valueTypes
{
    public class LongTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            long number;

            //Definition - 2
            long value1, value2, value3;

            //Definition - 3
            long minValue = -9223372036854775808 , maxValue = 9223372036854775807;
            Console.WriteLine("Min value long: {0} \nMax value long: {1}",
               minValue, maxValue);

            //Definition - 4
            long defaultValue = 0;
            Console.WriteLine("Default value long: " + defaultValue);
            long value = 46747484;
            Console.WriteLine("Value long: " + value);

        }
    }
}
