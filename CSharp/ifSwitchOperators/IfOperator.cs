﻿using System;

namespace CSharp.ifSwitchOperators
{
    public class IfOperator
    {
        public void Define()
        {
            Console.Write("Enter value1: ");
            int value1 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter value2: ");
            int value2 = Convert.ToInt32(Console.ReadLine());

            if (value1 > value2)
            {
                Console.WriteLine("Value1 is greater than value2");
            }
                


            Console.WriteLine("Enter value:");
            Console.Write("a = ");

            int a = Convert.ToInt32(Console.ReadLine());
            if (a % 2 == 0)
                Console.WriteLine("The number is pair!");
            else
                Console.WriteLine("The number is odd!");


            Console.WriteLine();
        }


        public void ConditionalOperator()
        {
            Console.Write("Enter time: ");
            int time = Convert.ToInt32(Console.ReadLine());

            if (time < 10)
            {
                Console.WriteLine("Good morning.");
            }
            else if (time < 20)
            {
                Console.WriteLine("Good day.");
            }
            else
            {
                Console.WriteLine("Good evening.");
            }

            Console.WriteLine();
        }


        public void Condition()
        {
            Console.WriteLine("Enter value: ");
            Console.Write("a = ");
            float a = Convert.ToSingle(Console.ReadLine());
            Console.Write("b = ");
            float b = Convert.ToSingle(Console.ReadLine());
            float F;
            if (b > 0)
                F = a + b;
            else if (b == 0)
                F = a;
            else
                F = a - b;
            Console.WriteLine("F = " + F);
        }
    }
}
