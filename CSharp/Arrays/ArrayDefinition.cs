﻿using System;

namespace CSharp.Arrays
{
    public class ArrayDefinition
    {
        public void Define()
        {
            // Define one-dimensional array
            int[] arr1;

            int[] arr2 = new int[5];

            string[] arr3 = new string[] { "c#", "asp.net core", "asp.net core mvc", "ado.net", "rest api" };
            
            double[] arr4 = new double[4] { 1, 3.5, 4, 8, };

            float[] arr5 = { 1, 4.6f, 8, 11, 34 };

            int[] arr6 = new int[3];
            arr6[0] = 12;
            arr6[1] = 45;
            arr6[2] = 100;

            for (int i = 0; i < arr3.Length; i++)
            {
                Console.WriteLine(arr3[i]);
            }

            for (int i = 0; i < arr4.Length; i++)
            {
                Console.WriteLine(arr4[i]);
            }

            // define multiple-dimensional array

            int[,] multiarray1 = new int[3, 4];

            int[,] multiArray2 = new int[3, 5]
            {
                { 1,2 ,3,4 ,5},
                {6,7,5,-4,23 },
                { 34, 100, 567,37,7},
            };

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.WriteLine("[{0},{1}] = {2}",i,j, multiArray2[i, j]);
                }
            }

            //define jagged array

            int[][] jaggedArray = new int[3][]
            {
                new int[] {3, 5,6},
                new int[] { 4,56,778,89},
                new int[]{-3, -4, -5}
            };

            for (int i = 0; i < jaggedArray.Length; i++)
            {
                for (int j = 0; j < jaggedArray[i].Length; j++)
                {
                    Console.WriteLine("[{0}][{1}] = {2}",i, j,jaggedArray[i][j]);
                }
            }
        }
    }
}
