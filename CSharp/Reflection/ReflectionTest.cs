﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CSharp.Reflection
{
    public class ReflectionTest
    {
        public void Define()
        {
            Type t2 = typeof(ReflectionTest);
            Console.WriteLine(t2);
            
            Type t = typeof(string);
            Console.WriteLine("Name : {0}", t.Name);
            Console.WriteLine("Full Name : {0}", t.FullName);
            Console.WriteLine("Namespace : {0}", t.Namespace);
            Console.WriteLine("Base Type : {0}", t.BaseType);

            int number = 35;
            Type t1 = typeof(int);
            Console.WriteLine(t1);
            Console.WriteLine(t1.BaseType);

            Assembly info = typeof(int).Assembly;
            Console.WriteLine(info);
        }
    }
}
