﻿using System;

namespace CSharp.Algorithms
{
    public class QuadraticEquations
    {
        public Tuple<double, double> FindQuatraticEquation(double a, double b, double c)
        {
            if (a != 0)
            {
                double d = Math.Pow(b, 2) - 4 * a * c;

                if (d > 0)
                {
                    double x1 = (-b - Math.Sqrt(d)) / (2 * a);
                    double x2 = (-b + Math.Sqrt(d)) / (2 * a);
                    return new Tuple<double, double>(x1, x2);
                }
                else if (d == 0)
                {
                    double x = -b / (2 * a);
                    return new Tuple<double, double>(x,x);
                }
                else
                {
                    throw new Exception("Tenliyin heqiqi koku yoxdur");
                }
            }
            else
            {
                throw new Exception("a-nin qiymeti 0 olmamalidir!");
            }
        }
    }
}
