﻿namespace CSharp.Algorithms
{
    public class GCF
    {
        // Find GCF
        public int FindGCF(int x, int y)
        {
            while (x != y)
            {
                if (x > y)
                {
                    x = x - y;
                }
                else if (y > x)
                {
                    y = y - x;
                }
            }
            return y;
        }

        //Find LCM
        public int FindLCM(int x, int y)
        {
            return x * y / FindGCF(x, y);
        }
    }
}
