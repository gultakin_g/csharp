﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace CSharp.Serilization.XmlSerilization
{
    public class XmlSerilizator
    {
        public static void WriteToFileAsText<T>(string fileName,string directoryPath,T data)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            bool existsDirectory = Directory.Exists(directoryPath);

            if (!existsDirectory)
            {
                Directory.CreateDirectory(directoryPath);
            }

            string filePath = directoryPath + "/" + fileName;

            bool existsFile = File.Exists(filePath);

            if (!existsFile)
            {
                File.Create(filePath);
            }

            using (TextWriter txtWriter = new StreamWriter(filePath))
            {
                serializer.Serialize(txtWriter, data);
            }
        }

        public static T ReadToFileAsText<T>(string filePath)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StreamReader reader = new StreamReader(filePath);
            return  (T) serializer.Deserialize(reader);
        }
    }
}
