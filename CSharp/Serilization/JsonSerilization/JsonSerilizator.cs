﻿using System.IO;
using System.Text.Json;

namespace CSharp.Serilization.JsonSerilization
{
    public class JsonSerilizator
    {
        
        public static void WriteToFileAsText<T>(string fileName, string directoryPath, T data)
        {
            string jsonData = JsonSerializer.Serialize(data);

            bool existsDirectory = Directory.Exists(directoryPath);

            if (!existsDirectory)
            {
                Directory.CreateDirectory(directoryPath);
            }

            string filePath = directoryPath + "/" + fileName;

            bool existsFile = File.Exists(filePath);

            if (!existsFile)
            {
                File.Create(filePath);
            }

            File.WriteAllText(filePath, jsonData);
        }

        public static T ReadToFileAsText<T>(string filePath)
        {
            string fileTextStudent = File.ReadAllText(filePath);
            return JsonSerializer.Deserialize<T>(fileTextStudent);
        }
    }
}
