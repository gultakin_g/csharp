﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace CSharp.Serilization
{
    [XmlRootAttribute("Student", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class Student
    {
        public Student()
        {
        }

        public Student(
            int id,
            string firstName,
            string lastName,
            string fatherName,
            DateTime dateOfBirth)
        {

        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FatherName { get; set; }
        public DateTime DateOfBirth { get; set; }

        public void Print()
        {
            Console.WriteLine("Hello from Student class!");
        }

        public override string ToString()
        {
            Console.WriteLine("Student: ");
            return $"Id = {Id},\n" +
                $"FirstName = {FirstName},\n" +
                $"LastName = {LastName},\n" +
                $"FatherName = {FatherName},\n" +
                $"Date of Birth = {DateOfBirth.ToLongDateString()}\n" +
                $"------------------------------------------------";
        }
    }
}
