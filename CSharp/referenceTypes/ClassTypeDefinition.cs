﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.referenceTypes
{
    public class ClassTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            Student student;

            //Definition - 2
            Student student1, student2;

            //Definition - 3
            Student student3 = new Student();
            student3.Set(
                "Gultakin",
                "Gasimova",
                23,
                "Junior Software Developer",
                "I'm a developer, I have no life"
                );
            student3.Print();
        }
    }
}
