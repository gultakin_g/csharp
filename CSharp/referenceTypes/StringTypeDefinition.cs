﻿using System;

namespace CSharp.referenceTypes
{
    public class StringTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            string name;

            //Definition - 2
            string sentence, message, alphabe;

            //Definition - 3
            string firstName = "Gultakin", lastName = "Gasimova", age = "33";
            Console.WriteLine("First name: " + firstName +
                              "\nLast name: " + lastName +
                              "\nAge: " + age);

            //Definition - 4
            string type = "string";
            string nullString = null; 
            Console.WriteLine("Type: {0} \nnull string: {1}", type,nullString);

            //Definition - 5
            string str = new string("New string");
            Console.WriteLine(str);
        }
    }
}
