﻿namespace CSharp.referenceTypes
{
    public interface IStudent
    {
        void Set(
            string firstName,
            string lastName,
            int age,
            string jobTitle,
            string description
            );
        void Print();
    }
}
