﻿using System;

namespace CSharp.referenceTypes
{
    public class Student : IStudent
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string JobTitle { get; set; }
        public string Description { get; set; }

        public void Set(
            string firstName,
            string lastName,
            int age,
            string jobTitle,
            string description
            )
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            JobTitle = jobTitle;
            Description = description;

        }

        public void Print()
        {
            Console.WriteLine(
                "First name: " + FirstName+
                "\nLast name: "+LastName+
                "\nAge: "+Age+
                "\nJob title: "+JobTitle+
                "\nDescriptio: "+Description
                );
        }

    }
}
