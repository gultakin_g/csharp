﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.referenceTypes
{
    public class DynamicTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            dynamic value;

            //Definition - 2
            dynamic value1, value2, value3;

            //Definition - 3
            dynamic type = "dynamic", number = 30;
            
            //Definition - 4
            dynamic citiesName = "Baku, Zaqatala, Sheki";
            dynamic cityNumber = "47";

            Console.WriteLine("Type: {0} \nValue: {1} " +
                              "\nCities name: {2} \nCity number: {3}",
                              type,number,citiesName,cityNumber);
        }
    }
}
