﻿using System;

namespace CSharp.referenceTypes
{
    public class ObjectTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            object obj;

            //Definition - 2
            object fixedRowCount,  message, number;

            //Definition - 3
            object type = "object", value = 5;
            Console.WriteLine("Type: {0} \nValue: {1}", type, value);

            //Definition  - 4
            object name = "Elliot";
            object age = 43;
            Console.WriteLine("Name: "+ name + "\nAge: " + age);

        }
    }
}
