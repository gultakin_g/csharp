﻿using System;
using System.Text;

namespace CSharp.referenceTypes
{
    public class StringBuilderTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            StringBuilder value;

            //Definition - 2
            StringBuilder number, generator;

            //Definition - 3
            StringBuilder type = new StringBuilder("String Builder");

            //Definition - 4
            StringBuilder age = new StringBuilder("4");

            Console.WriteLine("Type: {0} \nAge: {1}", type, age);

        }
    }
}
