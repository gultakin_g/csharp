﻿using System;

namespace CSharp.referenceTypes
{
    public class ValueTypesAsReferenceTypeDefinition
    {
        public void Define()
        {
            int number1 = 23;
            int number2 = 23;
            RefExample(ref number1, number2);
            Console.WriteLine("Ref example results:");
            Console.WriteLine("Number1 = " + number1);
            Console.WriteLine("Number1 = " + number2);

            int number3;
            int number4 = 55;
            OutExample(out number3, number4);
            Console.WriteLine("Out example results:");
            Console.WriteLine("Number3 = " + number3);
            Console.WriteLine("Number4 = " + number4);
        }

        private void RefExample(ref int value1, int value2)
        {
            value1++;
            value2++;
        }

        private void OutExample(out int value1, int value2)
        {
            value1 = 40;
            value2 = 40;
        }
    }
}
