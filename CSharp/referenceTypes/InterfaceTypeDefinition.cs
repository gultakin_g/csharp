﻿namespace CSharp.referenceTypes
{
    public class InterfaceTypeDefinition
    {
        public void Define()
        {
            //Definition - 1
            IStudent student;

            //Definition - 2
            IStudent student1, student2;

            //Definition - 3
            IStudent student3 = new Student();
            student3.Set(
                "Gultakin",
                "Gasimova",
                23,
                "Junior Software Developer",
                "I'm a developer, I have no life"
                );
            student3.Print();
        }
    }
}
