﻿using System;

namespace CSharp.helloWorld
{
    public class HelloWorld
    {
        public void PrintHelloWorld()
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("It's me, Gultakin Gasimova");
        }
    }
}
