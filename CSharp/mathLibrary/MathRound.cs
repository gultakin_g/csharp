﻿using System;

namespace CSharp.mathLibrary
{
    public class MathRound
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Round() example");

            double value1 = Math.Round(3.2);
            Console.WriteLine("Result: Math.Round(3.2) = "+value1);

            double value2 = Math.Round(4.6);
            Console.WriteLine("Result: Math.Round(4.6) = " + value2);

        }
    }
}
