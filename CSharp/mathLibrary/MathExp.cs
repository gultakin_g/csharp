﻿using System;

namespace CSharp.mathLibrary
{
    public class MathExp
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Exp() example");

            double value1 = Math.Exp(2);
            Console.WriteLine("Result: Math.Exp(2) = " + value1);

            double value2 = Math.Exp(3);
            Console.WriteLine("Result: Math.Exp(3) = " + value2);
        }
    }
}
