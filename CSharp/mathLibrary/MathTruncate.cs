﻿using System;

namespace CSharp.mathLibrary
{
    public class MathTruncate
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Truncate() example");

            double value1 = Math.Truncate(3.14);
            Console.WriteLine("Result: Math.Truncate(3.14) = "+value1);

            double value2 = Math.Truncate(45.3);
            Console.WriteLine("Result: Math.Truncate(45.3) = " + value2);
        }
    }
}
