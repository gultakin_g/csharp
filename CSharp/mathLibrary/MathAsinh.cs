﻿using System;

namespace CSharp.mathLibrary
{
    public class MathAsinh
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Asinh() example");

            int value1 = (int)Math.Asinh(5);
            Console.WriteLine("Result: Asinh(5) = {0}", value1);

            int value2 = (int)Math.Asinh(7);
            Console.WriteLine("Result: Asinh(7) = {0}", value2);
        }
    }
}
