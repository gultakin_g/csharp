﻿using System;

namespace CSharp.mathLibrary
{
    public class MathLog
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Log() example");

            double value1 = Math.Log(3);
            Console.WriteLine("Result: Math.Log(1) = " + value1);

            double value2 = Math.Log(7);
            Console.WriteLine("Result: Math.Log(7) = " + value2);
        }
    }
}
