﻿using System;

namespace CSharp.mathLibrary
{
    public class MathCosh
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Cosh() example");

            double value1 = Math.Cosh(1);
            Console.WriteLine("Result: Math.Cosh(1) = " + value1);

            double value2 = Math.Cosh(0);
            Console.WriteLine("Result: Math.Cosh(0) = " + value2);
        }
    }
}
