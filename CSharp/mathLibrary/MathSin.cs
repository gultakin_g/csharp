﻿using System;

namespace CSharp.mathLibrary
{
    public class MathSin
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Sin() example");

            double value1 = Math.Sin(1);
            Console.WriteLine("Result: Math.Sin(1) = " + value1);

            double value2 = Math.Sin(0);
            Console.WriteLine("Result: Math.Sin(0) = " + value2);
        }
    }
}
