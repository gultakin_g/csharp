﻿using System;

namespace CSharp.mathLibrary
{
    public class MathCbrt
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Cbrt() example");

            double value1 = Math.Cbrt(27);
            Console.WriteLine("Result: Math.Cbrt(27) = {0}",value1);

            double value2 = Math.Cbrt(64);
            Console.WriteLine("Result: Math.Cbrt(64) = {0}", value2);

            double value3 = Math.Cbrt(125);
            Console.WriteLine("Result: Math.Cbrt(125) = {0}", value3);
        }
    }
}
