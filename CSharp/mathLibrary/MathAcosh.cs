﻿using System;

namespace CSharp.mathLibrary
{
    public class MathAcosh
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Acosh() example");

            int value1 = (int)Math.Acosh(2);
            Console.WriteLine("Result: Acosh(2) = {0}",value1);

            int value2 = (int)Math.Acosh(4);
            Console.WriteLine("Result: Acosh(4) = {0}", value2);
        }
    }
}
