﻿using System;

namespace CSharp.mathLibrary
{
    public class MathAbs
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Abs() example");
            int value1 = Math.Abs(-42);
            Console.WriteLine("Result: |-42| = {0} ", value1);

            float value2 = Math.Abs(-6.7f);
            Console.WriteLine("Result: |-6.7| = {0} ", value2);

            double value3 = Math.Abs(34.4);
            Console.WriteLine("Result: |-34.4| = {0} ", value3);
        }
    }
}
