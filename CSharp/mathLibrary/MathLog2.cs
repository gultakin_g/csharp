﻿using System;

namespace CSharp.mathLibrary
{
    public class MathLog2
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Log2() example");

            double value1 = Math.Log2(2);
            Console.WriteLine("Result: Math.Log2(2) = " + value1);

            double value2 = Math.Log2(4);
            Console.WriteLine("Result: Math.Log2(4) = " + value2);
        }
    }
}
