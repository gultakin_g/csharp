﻿using System;

namespace CSharp.mathLibrary
{
    public class MathSqrt
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Sqrt() example");

            double value1 = Math.Sqrt(16);
            Console.WriteLine("Result: Math.Sqrt(16) = {0}", value1);

            double value2 = Math.Sqrt(64);
            Console.WriteLine("Result: Math.Sqrt(64) = {0}", value2);
        }
    }
}
