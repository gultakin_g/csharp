﻿using System;

namespace CSharp.mathLibrary
{
    public class MathFloor
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Floor() example");

            decimal value1 = Math.Floor(2.6m);
            Console.WriteLine("Result: Math.Floor(2.6) = " + value1);

            decimal value2 = Math.Floor(3.2m);
            Console.WriteLine("Result: Math.Floor(3.2) = " + value2);
        }
    }
}
