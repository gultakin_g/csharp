﻿using System;

namespace CSharp.mathLibrary
{
    public class MathCeiling
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Ceiling() example");

            double value1 = Math.Ceiling(4.6);
            Console.WriteLine("Result: Math.Ceiling(4.6) = " + value1);

            double value2 = Math.Ceiling(3.2);
            Console.WriteLine("Result: Math.Ceiling(3.2) = " + value2);
        }
    }
}
