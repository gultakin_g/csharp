﻿using System;

namespace CSharp.mathLibrary
{
    public class MathMax
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Max() example");
            int value1 = 20;
            int value2 = 12;
            int max = Math.Max(value1, value2);
            Console.WriteLine("Maximum value: {0}", max);
        }
    }
}
