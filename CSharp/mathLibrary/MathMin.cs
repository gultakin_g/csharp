﻿using System;

namespace CSharp.mathLibrary
{
    public class MathMin
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Min() example");
            int value1 = 20;
            int value2 = 12;
            int minValue = Math.Min(value1, value2);
            Console.WriteLine("Minimum value: ", minValue);
        }
    }
}
