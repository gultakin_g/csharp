﻿using System;

namespace CSharp.mathLibrary
{
    public class MathAsin
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Asin() example");

            int value1 = (int)Math.Asin(1);
            Console.WriteLine("Result: Asin(1) = {0}", value1);

            int value2 = (int)Math.Asin(2);
            Console.WriteLine("Result: Asin(2) = {0}", value2);
        }
    }
}
