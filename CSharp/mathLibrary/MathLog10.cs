﻿using System;

namespace CSharp.mathLibrary
{
    public class MathLog10
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Log10() example");

            double value1 = Math.Log10(1);
            Console.WriteLine("Result: Math.Log10(1) = " + value1);

            double value2 = Math.Log10(10);
            Console.WriteLine("Result: Math.Log10(10) = " + value2);
        }
    }
}
