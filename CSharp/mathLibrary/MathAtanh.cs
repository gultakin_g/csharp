﻿using System;

namespace CSharp.mathLibrary
{
    public class MathAtanh
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Atanh() example");

            int value1 = (int)Math.Atanh(0);
            Console.WriteLine("Result: Atanh(0) = {0}", value1);

            int value2 = (int)Math.Atanh(1);
            Console.WriteLine("Result: Atanh(1) = {0}", value2);
        }
    }
}
