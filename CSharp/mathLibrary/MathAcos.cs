﻿using System;

namespace CSharp.mathLibrary
{
    public class MathAcos
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Acos() example");
            int value1 = (int)Math.Acos(2);
            Console.WriteLine("Result: Acos(1) = {0}", value1);

            int value2 = (int)Math.Acos(1);
            Console.WriteLine("Result: Acos(1) = {0}", value2);
        }
    }
}
