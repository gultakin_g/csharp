﻿using System;

namespace CSharp.mathLibrary
{
    public class MathSinh
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Sinh() example");

            double value1 = Math.Sinh(5);
            Console.WriteLine("Result: Math.Sinh(5) = " + value1);

            double value2 = Math.Sinh(3);
            Console.WriteLine("Result: Math.Sinh(3) = " + value2);
        }
    }
}
