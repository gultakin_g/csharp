﻿using System;

namespace CSharp.mathLibrary
{
    public class MathPI
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.PI() example");

            double value1 = Math.PI;
            Console.WriteLine("PI = " + value1);
        }
    }
}
