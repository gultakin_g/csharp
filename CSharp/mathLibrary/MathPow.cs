﻿using System;

namespace CSharp.mathLibrary
{
    public class MathPow
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Pow() example");

            double value1 = Math.Pow(45,2);
            Console.WriteLine("Result: Math.Pow(45, 2) = {0}",value1);

            double value2 = Math.Pow(18, 2);
            Console.WriteLine("Result: Math.Pow(18, 2) = {0}", value2);
        }
    }
}
