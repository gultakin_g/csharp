﻿using System;


namespace CSharp.mathLibrary
{
    public class MathSign
    {
        public void MathLibrary()
        {
            int value1 = Math.Sign(-3);
            Console.WriteLine("Result: Math.Sign(-3) = "+value1);

            int value2 = Math.Sign(0);
            Console.WriteLine("Result: Math.Sign(0) = " + value2);

            int value3 = Math.Sign(3);
            Console.WriteLine("Result: Math.Sign(3) = " + value3);
        }
    }
}
