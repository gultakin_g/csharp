﻿using System;

namespace CSharp.mathLibrary
{
    public class MathAtan
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Atan() example");

            int value1 = (int)Math.Atan(1);
            Console.WriteLine("Result: Atan(1) = {0}", value1);

            int value2 = (int)Math.Atan(2);
            Console.WriteLine("Result: Atan(3) = {0}", value2);
        }
    }
}
