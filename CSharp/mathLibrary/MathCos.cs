﻿using System;

namespace CSharp.mathLibrary
{
    public class MathCos
    {
        public void MathLibrary()
        {
            Console.WriteLine("Math.Cos() example");

            double value1 = Math.Cos(1);
            Console.WriteLine("Result: Math.Cos(1) = " + value1);

            double value2 = Math.Cos(0);
            Console.WriteLine("Result: Math.Cos(0) = " + value2);
        }
    }
}
