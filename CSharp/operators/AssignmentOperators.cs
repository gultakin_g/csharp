﻿using System;

namespace CSharp.operators
{
    public class AssignmentOperators
    {
        public void Define()
        {
            Console.WriteLine("Assingnment Operators");

            Console.Write("Enter value1: ");
            double value1 = Convert.ToDouble(Console.ReadLine());

            double value2;

            value2 = value1;
            Console.WriteLine("Line-1  = Value of value2 = {0}", value2);

            value2 += value1;
            Console.WriteLine("Line-2  += Value of value2 = {0}", value2);

            value2 -= value1;
            Console.WriteLine("Line-3  -= Value of value2 = {0}", value2);

            value2 /= value1;
            Console.WriteLine("Line-4  /= Value of value2 = {0}", value2);

            value2 *= value1;
            Console.WriteLine("Line-5  *= Value of value2 = {0}", value2);

            value2 %= value1;
            Console.WriteLine("Line-6  %= Value of value2 = {0}", value2);
        }
    }
}
