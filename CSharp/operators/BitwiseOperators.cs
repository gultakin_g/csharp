﻿using System;

namespace CSharp.operators
{
    public class BitwiseOperators
    {
        public void Define()
        {
            Console.WriteLine("Bitwise operators");

            Console.Write("Enter value1: ");
            int value1 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter value2: ");
            int value2 = Convert.ToInt32(Console.ReadLine());

            int result;
            result = value1 & value2;
            Console.WriteLine("\nBitwise AND: " + result);

            result = value1 | value2;
            Console.WriteLine("Bitwise OR: " + result);

            result = value1 ^ value2;
            Console.WriteLine("Bitwise XOR: " + result);

            result = ~value1;
            Console.WriteLine("Bitwise Complement: " + result);
        }
    }
}
