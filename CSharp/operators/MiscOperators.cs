﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp.operators
{
    public class MiscOperators
    {
        public void Define()
        {
            /* example of sizeof operator */
            Console.WriteLine("Miscellaneous operators");
            Console.WriteLine("The size of int is {0}", sizeof(int));
            Console.WriteLine("The size of short is {0}", sizeof(short));
            Console.WriteLine("The size of double is {0}", sizeof(double));

            /* example of ternary operator */
            int a, b;
            a = 10;
            b = (a == 1) ? 20 : 30;
            Console.WriteLine("Value of b is {0}", b);
        }
    }
}
