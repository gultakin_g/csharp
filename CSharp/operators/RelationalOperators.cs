﻿using System;

namespace CSharp.operators
{
    public class RelationalOperators
    {
        public void Define()
        {
            Console.WriteLine("Relational operators.");

            Console.Write("Enter value1: ");
            int value1 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter value2: ");
            int value2 = Convert.ToInt32(Console.ReadLine());

            if (value1 == value2)
                Console.WriteLine("Value1 is equal to value2");
            else
                Console.WriteLine("Value1 is not equal to value2");

            if (value1 < value2)
                Console.WriteLine("Value1 is less than value2");
            else
                Console.WriteLine("Value1 is not less than value2");

            if (value1 > value2)
                Console.WriteLine("Value1 is greater than value2");
            else
                Console.WriteLine("Value1 is not greater than value2");

            if (value1 <= value2)
                Console.WriteLine("Value1 is either less than or equal to  value2");

            if (value1 >= value2)
                Console.WriteLine("Value1 is either greater than or equal to value2");
        }
    }
}
