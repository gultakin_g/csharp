﻿using System;

namespace CSharp.operators
{
    public class ArithmeticOperators
    {
        public void Define()
        {
            Console.WriteLine("Arithmetic operators.");
            Console.Write("Enter value1: ");
            double value1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter value2: ");
            double value2 = Convert.ToDouble(Console.ReadLine());

            double sum = value1 + value2;
            Console.WriteLine("Sum: {0}", sum);

            double subtraction = value1 - value2;
            Console.WriteLine("Subtraction: " + subtraction);

            double division = value1 / value2;
            Console.WriteLine("Division: {0}", division);

            double multiplication = value1 * value2;
            Console.WriteLine("Multiplication: " + multiplication);

            double modulus = value1 % value2;
            Console.WriteLine("Modulus : " + modulus);

            double increment = value1++;
            Console.WriteLine("Increment: " + increment);

            double decrement = value2--;
            Console.WriteLine("Decrement: " + decrement);
        }
    }
}
