﻿using System;

namespace CSharp.operators
{
    public class LogicalOperators
    {
        public void Define()
        {
            Console.WriteLine("Logical operators");

            Console.Write("Enter first name: ");
            string firstName = Console.ReadLine();

            Console.Write("Enter last name: ");
            string lastName = Console.ReadLine();

            if (firstName == "Gultakin" && lastName == "Gasimova")
            {
                Console.WriteLine("Login Successful");
            }
            else
            {
                Console.WriteLine("Unauthorised access");
            }

            Console.WriteLine();

            Console.Write("Enter user name: ");
            string userName = Console.ReadLine();

            Console.Write("Enter user password: ");
            string userPassword = Console.ReadLine();

            if ((userName == "Gultakin" || userName == "Jeyhun") && (userPassword == "demopass"))
            {
                Console.WriteLine("\nLogin Successful.");
            }
            else
            {
                Console.WriteLine("\nUnauthorised Access. Aborting...");
            }

            if (!(userName == "Steven" && userPassword == "demopass"))
            {
                Console.WriteLine("\nLogin Successful");
            }
            else
            {
                Console.WriteLine("\nUnauthorised Access. Aborting...");
            }
        }
    }
}
