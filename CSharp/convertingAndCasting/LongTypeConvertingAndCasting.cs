﻿using System;

namespace CSharp.convertingAndCasting
{
    public class LongTypeConvertingAndCasting
    {
        public void ConvertType()
        {
            //Converting 
            Console.WriteLine("Long type converting and casting:");
            long value1 = Convert.ToInt64("9");
            Console.WriteLine("Value1 = " + value1);

            //Casting
            var value2 = (long)8;
            Console.WriteLine("Value2 = {0}", value2);

            //Parse
            long value3 = long.Parse("3");
            Console.WriteLine("Value3 = {0}", value3);

            //TryParse
            long.TryParse("50", out long value4);
            Console.WriteLine("Value4 = {0}", value4);

            //Boxing
            Console.WriteLine("Boxing:");
            long num = 2345;
            object obj = num;
            Console.WriteLine("Value - type value of num is : {0}", num);
            Console.WriteLine("Object - type value of num is : {0}", obj);

            //Unboxing
            Console.WriteLine("Unboxing:");
            object obj1 = 145678;
            long value5 = (long)(int)obj1;
            Console.WriteLine("Value of i is: {0}", value5);
        }
    }
}
