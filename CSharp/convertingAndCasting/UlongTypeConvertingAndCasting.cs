﻿using System;

namespace CSharp.convertingAndCasting
{
    public class UlongTypeConvertingAndCasting
    {
        public void ConvertType()
        {
            Console.WriteLine("Ulong type converting and casting:");

            //Converting
            ulong value1 = Convert.ToUInt64("12300");
            Console.WriteLine("Value1 = " + value1);

            //Casting
            float value2 = (ulong)1345.8;
            Console.WriteLine("Value2: " + value2);

            //Parse
            string str = "975445";
            ulong value3 = ulong.Parse(str);

            //TryParse
            var number = "89765";
            ulong value4;
            bool b = ulong.TryParse(number, out value4);
            Console.WriteLine("String is a numeric representation: {0}", b);

            //Boxing
            Console.WriteLine("Boxing:");
            ulong value5 = 132256;
            object obj = value5;
            Console.WriteLine("Value - type value of num is : {0}", value5);
            Console.WriteLine("Object - type value of num is : {0}", obj);

            //Unboxing
            Console.WriteLine("Unboxing:");
            object obj1 = 6728;
            ulong value6 = (ulong)(int)obj1;
            Console.WriteLine("Value of i is: {0}", value6);
        }
    }
}
