﻿using System;

namespace CSharp.convertingAndCasting
{
    public class SbyteTypeConvertingAndCasting
    {
        public void ConvertType()
        {
            Console.WriteLine("Sbyte type converting and casting:");

            //Converting
            sbyte value1 = Convert.ToSByte("10");
            Console.WriteLine("Value1 = " + value1);

            //Casting
            double value2 = (sbyte)44.9;
            Console.WriteLine("Value2: " + value2);

            //Parse
            string str = "-45";
            sbyte value3 = sbyte.Parse(str);

            //TryParse
            string str1 = "123";
            sbyte value4;
            bool b = sbyte.TryParse(str1, out value4);
            Console.WriteLine("String is a numeric representation: {0}", b);

            //Boxing
            Console.WriteLine("Boxing:");
            sbyte value5 = 126;
            object obj = value5;
            Console.WriteLine("Value - type value of num is : {0}", value5);
            Console.WriteLine("Object - type value of num is : {0}", obj);

            //Unboxing
            Console.WriteLine("Unboxing:");
            object obj1 = 28;
            sbyte value6 = (sbyte)(int)obj1;
            Console.WriteLine("Value of i is: {0}", value6);
        }
    }
}
