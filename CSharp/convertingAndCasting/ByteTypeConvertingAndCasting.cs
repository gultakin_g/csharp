﻿using System;

namespace CSharp.convertingAndCasting
{
    public class ByteTypeConvertingAndCasting
    {
        public void ConvertType()
        {
            //Converting 
            Console.WriteLine("Byte type converting and casting:");
            byte value1 = Convert.ToByte("65");
            Console.WriteLine("Value1 = "+value1);

            //Casting
            var value2 = (byte)34;
            Console.WriteLine("Value2 = {0}",value2);

            //Parse
            byte value3  = byte.Parse("89");
            Console.WriteLine("Value3 = {0}",value3);

            //TryParse
            byte.TryParse("254",out byte value4);
            Console.WriteLine("Value4 = {0}", value4);

            //Boxing
            Console.WriteLine("Boxing:");
            byte num = 56;
            object obj = num;
            Console.WriteLine("Value - type value of num is : {0}", num);
            Console.WriteLine("Object - type value of num is : {0}", obj);

            //Unboxing
            Console.WriteLine("Unboxing:");
            object obj1 = 13;
            byte value5 = (byte)(int)obj1;
            Console.WriteLine("Value of i is: {0}",obj1);
        }

    }
}
