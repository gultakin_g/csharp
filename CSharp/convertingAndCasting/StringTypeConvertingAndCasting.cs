﻿using System;

namespace CSharp.convertingAndCasting
{
    public class StringTypeConvertingAndCasting
    {
        public void ConvertType()
        {
            //Converting
            int value1 = 23;
            string number = Convert.ToString(value1);

            //Casting
            double value2 = 34.5;
            string str = value2.ToString();
        }
    }
}
