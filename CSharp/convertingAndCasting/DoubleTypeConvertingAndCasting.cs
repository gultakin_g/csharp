﻿using System;

namespace CSharp.convertingAndCasting
{
    public class DoubleTypeConvertingAndCasting
    {
        public void ConvertType()
        {
            //Converting 
            Console.WriteLine("Double type converting and casting:");
            double number1 = Convert.ToSingle("1.77");
            Console.WriteLine("Value1 = " + number1);

            //Casting
            var value2 = (double)0.99;
            Console.WriteLine("Value2 = {0}", value2);

            //Parse
            double value3 = double.Parse("8943");
            Console.WriteLine("Value3 = {0}", value3);

            //TryParse
            double.TryParse("1987", out double value4);
            Console.WriteLine("Value4 = {0}", value4);

            //Boxing
            Console.WriteLine("Boxing:");
            double num = 45.5;
            object obj = num;
            Console.WriteLine("Value - type value of num is : {0}", num);
            Console.WriteLine("Object - type value of num is : {0}", obj);

            //Unboxing
            Console.WriteLine("Unboxing:");
            object obj1 = 1.6;
            double value5 = (double)obj1;
            Console.WriteLine("Value of i is: {0}", value5);
        }
    }
}
