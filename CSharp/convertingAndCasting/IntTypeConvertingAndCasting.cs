﻿using System;

namespace CSharp.convertingAndCasting
{
    public class IntTypeConvertingAndCasting
    {
        public void ConvertType()
        {
            //Converting 
            Console.WriteLine("Int type converting and casting:");
            int value1 = Convert.ToInt32("25");
            Console.WriteLine("Value1 = " + value1);

            //Casting
            var value2 = (int)34485;
            Console.WriteLine("Value2 = {0}", value2);

            //Parse
            int value3 = int.Parse("845");
            Console.WriteLine("Value3 = {0}", value3);

            //TryParse
            int.TryParse("978211", out int value4);
            Console.WriteLine("Value4 = {0}", value4);

            //Boxing
            Console.WriteLine("Boxing:");
            int num = 53416;
            object obj = num;
            Console.WriteLine("Value - type value of num is : {0}", num);
            Console.WriteLine("Object - type value of num is : {0}", obj);

            //Unboxing
            Console.WriteLine("Unboxing:");
            object obj1 = 4;
            int value5 = (int)obj1;
            Console.WriteLine("Value of i is: {0}", value5);
        }
    }
}
