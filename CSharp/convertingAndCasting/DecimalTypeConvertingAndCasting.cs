﻿using System;

namespace CSharp.convertingAndCasting
{
    public class DecimalTypeConvertingAndCasting
    {
        public void ConvertType()
        {
            //Converting 
            Console.WriteLine("Decimal type converting and casting:");
            decimal number1 = Convert.ToDecimal("56.86767");
            Console.WriteLine("Value1 = " + number1);

            //Casting
            var value2 = (decimal)-5.345m;
            Console.WriteLine("Value2 = {0}", value2);

            //Parse
            decimal value3 = decimal.Parse("56.8");
            Console.WriteLine("Value3 = {0}", value3);

            //TryParse
            decimal.TryParse("2000", out decimal value4);
            Console.WriteLine("Value4 = {0}", value4);

            //Boxing
            Console.WriteLine("Boxing:");
            decimal num = 234.234m;
            object obj = num;
            Console.WriteLine("Value - type value of num is : {0}", num);
            Console.WriteLine("Object - type value of num is : {0}", obj);

            //Unboxing
            Console.WriteLine("Unboxing:");
            object obj1 = 45332m;
            decimal value5 = (decimal)obj1;
            Console.WriteLine("Value of i is: {0}", value5);
        }
    }
}
