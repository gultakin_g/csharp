﻿using System;

namespace CSharp.convertingAndCasting
{
    public class UshortConvertingAndCasting
    {
        public void ConvertType()
        {
            Console.WriteLine("Ushort type converting and casting:");

            //Converting
            ushort value1 = Convert.ToUInt16("140");
            Console.WriteLine("Value1 = " + value1);

            //Casting
            double value2 = (ushort)3.4;
            Console.WriteLine("Value2: " + value2);

            //Parse
            string str = "64";
            ushort value3 = ushort.Parse(str);

            //TryParse
            string str1 = "2";
            ushort value4;
            bool b = ushort.TryParse(str1, out value4);
            Console.WriteLine("String is a numeric representation: {0}", b);

            //Boxing
            Console.WriteLine("Boxing:");
            ushort value5 = 22;
            object obj = value5;
            Console.WriteLine("Value - type value of num is : {0}", value5);
            Console.WriteLine("Object - type value of num is : {0}", obj);

            //Unboxing
            Console.WriteLine("Unboxing:");
            object obj1 = 8;
            ushort value6 = (ushort)(int)obj1;
            Console.WriteLine("Value of i is: {0}", value6);
        }
    }
}
