﻿using System;

namespace CSharp.convertingAndCasting
{
    class FloatTypeConvertingAndCasting
    {
        public void ConvertType()
        {
            //Converting 
            Console.WriteLine("Float type converting and casting:");
            float number1 = Convert.ToSingle("7.8");
            Console.WriteLine("Value1 = " + number1);

            //Casting
            var value2 = (float)8.9f;
            Console.WriteLine("Value2 = {0}", value2);

            //Parse
            float value3 = float.Parse("456");
            Console.WriteLine("Value3 = {0}", value3);

            //TryParse
            float.TryParse("500", out float value4);
            Console.WriteLine("Value4 = {0}", value4);

            //Boxing
            Console.WriteLine("Boxing:");
            float num = 33.3f;
            object obj = num;
            Console.WriteLine("Value - type value of num is : {0}", num);
            Console.WriteLine("Object - type value of num is : {0}", obj);

            //Unboxing
            Console.WriteLine("Unboxing:");
            object obj1 = -4.4;
            float value5 =(float) Convert.ToDouble(obj1);
            Console.WriteLine("Value of i is: {0}", value5);
        }
    }
}
