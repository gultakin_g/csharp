﻿using System;

namespace CSharp.convertingAndCasting
{
    public class ShortTypeConvertingAndCasting
    {
        public void ConvertType()
        {
            //Convert 
            Console.WriteLine("Short type converting and casting:");
            short value1 = Convert.ToInt16("456");
            Console.WriteLine("Value: " + value1);

            //Casting
            var value2 = (short)568;
            Console.WriteLine("Value2 = {0}", value2);

            //Parse
            short value3 = short.Parse("2349");
            Console.WriteLine("Value3 = {0}", value3);

            //TryParse
            short.TryParse("254", out short value4);
            Console.WriteLine("Value4 = {0}", value4);

            //Boxing
            Console.WriteLine("Boxing");
            short num = 485;
            object obj = num;
            Console.WriteLine("Value - type value of num is : {0}", num);
            Console.WriteLine("Object - type value of num is : {0}", obj);

            //Unboxing
            Console.WriteLine("Unboxing");
            object obj1 = 139;
            short value5 = (short)(int)obj1;
            Console.WriteLine("Value of i is: {0}", value5);
        }
    }
}
