﻿using System;

namespace CSharp.convertingAndCasting
{
    public class UintTypeConvertingAndCasting
    {
        public void ConvertType()
        {
            Console.WriteLine("Uint type converting and casting:");

            //Converting
            uint value1 = Convert.ToUInt32("12300");
            Console.WriteLine("Value1 = " + value1);

            //Casting
            float value2 = (uint)1345.8;
            Console.WriteLine("Value2: " + value2);

            //Parse
            string str = "975445";
            uint value3 = uint.Parse(str);

            //TryParse
            var number = "89765";
            uint value4;
            bool b = uint.TryParse(number, out value4);
            Console.WriteLine("String is a numeric representation: {0}", b);

            //Boxing
            Console.WriteLine("Boxing:");
            uint value5 = 132256;
            object obj = value5;
            Console.WriteLine("Value - type value of num is : {0}", value5);
            Console.WriteLine("Object - type value of num is : {0}", obj);

            //Unboxing
            Console.WriteLine("Unboxing:");
            object obj1 = 6728;
            uint value6 = (uint)(int)obj1;
            Console.WriteLine("Value of i is: {0}", value6);
        }
    }
}
