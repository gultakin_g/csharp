﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Xml.Serialization;
using CSharp.Serilization;
using CSharp.Serilization.JsonSerilization;
using CSharp.Serilization.XmlSerilization;

namespace CSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student()
            {
                    Id = 1,
                    FirstName = "Gultakin",
                    LastName = "Gasimova",
                    FatherName = "Beydulla",
                    DateOfBirth = DateTime.Parse("03-14-1997")
            };

            XmlSerializer serializer = new XmlSerializer(typeof(Student));

            string directoryPath = "D:/IO/XmlSerilization";
            string fileName = "xmlSerilizator.xml";

            XmlSerilizator.WriteToFileAsText(fileName, directoryPath, student);

            Student student1 = XmlSerilizator.ReadToFileAsText<Student>(directoryPath + "/" + fileName);
            Console.WriteLine(student1);
        }
    }
}
