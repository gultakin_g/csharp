# CSharp

C# Console Application

1. helloWorld - my first code.
2. valueTypes - Definition value types.
3. referenceTypes - Definition reference types.
4. convertingAndCasting - Converting and casting data type
5. mathLibrary - Using Math Lib's all methods and properties
6. operators - Define arithmetic, relational, logical, bitwise, assignment, misc operators